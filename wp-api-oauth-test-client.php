<?php

require_once( '../../../wp-config.php' );
require_once( __DIR__ . '/autoloader.php' );

$request_options = get_option( 'wprat_options_name' );

$key           = isset( $request_options['key'] ) ? trim( $request_options['key'] ) : '';
$secret        = isset( $request_options['key'] ) ? trim( $request_options['secret'] ) : '';
$wp_api_domain = isset( $request_options['key'] ) ? sanitize_text_field( $request_options['uri_request'] ) : '';
$wp_api_path   = '/wp-json/wp/v2';
$wp_api_domain = 'http://' . $wp_api_domain;

$oauth_config = array(
	'key'            => $key,
	'secret'         => $secret,
	'wp_api_domain'  => $wp_api_domain,
	'wp_api_path'    => $wp_api_path,
	'uri_request'    => $wp_api_domain . '/oauth1/request',
	'uri_authorize'  => $wp_api_domain . '/oauth1/authorize',
	'uri_access'     => $wp_api_domain . '/oauth1/access',
	'uri_user'       => $wp_api_domain . $wp_api_path . '/users/me?context=edit',
	// 'embed' context excludes roles and capabilities, so use 'edit' to determine if publishing and uploads are allowed.
	'oauth_callback' => 'http://wordpress.loc/wp-content/plugins/wp-rest-api-test/wp-api-oauth-test-client.php'
);

$method = isset( $_GET['method'] ) ? sanitize_text_field( $_GET['method'] ) : '';
// Update section.
if ( isset( $_GET['logout'] ) && sanitize_text_field( $_GET['logout'] ) == 1 ) {
	setcookie( "access_token", $access_token, time() - 1, "/" );
	setcookie( "access_token_secret", $access_token_secret, time() - 1, "/" );
	setcookie( "user_object", json_encode( $user_object ), time() - 1, "/" );
	setcookie( "oauth_token_secret", "", time() - 1, "/" );
	header( 'Location: ' . $_SERVER['PHP_SELF'] );
}

$auth = new OAuthWP( $oauth_config );

// Pick up url query params after the oauth_callback after request token generation. (Also added check to make sure we're coming back from the OAuth server host)
//if(isset( $_REQUEST['oauth_token'] ) && isset( $_REQUEST['oauth_verifier'] ) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == parse_url($oauth_config['uri_request'], PHP_URL_HOST)  ){
if ( isset( $_REQUEST['oauth_token'] ) && isset( $_REQUEST['oauth_verifier'] ) ) {
	// Back from Authorisation. Now Generate Access Tokens for this user
	// Generate access tokens param string
	// Add the required 'oauth_verifier' parameter
	$request_data        = array(
		'oauth_verifier' => $_REQUEST['oauth_verifier'],
	);
	$temp_secret         = $_COOKIE['oauth_token_secret']; // from /request token leg
	$access_token_string = $auth->oauthRequest( $oauth_config['uri_access'], 'POST', $_REQUEST['oauth_token'], $temp_secret, $request_data ); // no token secret yet...
	parse_str( $access_token_string, $access_tokens );
	if ( ! isset( $access_tokens['oauth_token'] ) ) {
		echo '<h3>' . __( 'ERROR: Failed to get access tokens', 'wprat' ) . '</h3>';
		print_r( $access_tokens );
		echo '<hr>';
		print_r( $access_token_string );
		exit;
	}
	$access_token        = $access_tokens['oauth_token'];
	$access_token_secret = $access_tokens['oauth_token_secret'];
	// Verify user by getting currently looged in user daya from /wp-json/users/me
	$user_object = json_decode( $auth->oauthRequest( $oauth_config['uri_user'], 'GET', $access_token, $access_token_secret ) );
	// Store information in a cookie for when the page is reloaded
	setcookie( "access_token", $access_token, time() + ( 3600 * 72 ), "/" );                    // expire in 72 hours...
	setcookie( "access_token_secret", $access_token_secret, time() + ( 3600 * 72 ), "/" );       // expire in 72 hours...
	setcookie( "user_object", json_encode( $user_object ), time() + ( 3600 * 72 ), "/" );         // expire in 72 hours...
	// Clear the temp cookie
	setcookie( "oauth_token_secret", "", time() - 1, "/" );
	// Reload the page
	header( 'Location: ' . $_SERVER['PHP_SELF'] );
	exit;
}

if ( isset( $_COOKIE['access_token'] ) && isset( $_COOKIE['access_token_secret'] ) && isset( $_COOKIE['user_object'] ) ) {

	// LOGGED-IN : Visitor already appears to have the required cookies set.
	$u = json_decode( $_COOKIE['user_object'], TRUE );
	if( is_object( $u ) ) {
		$av = $u->avatar_urls;
		foreach ( $av as $key => $value ) {
			if ( $key == 48 ) { // medium thumbnail
				$av = $value;
			}
		}
		echo '<h3><img style="margin:6px;width:50px;height:50px;float:left;vertical-align:middle;" src="' . $av . '">' . __( 'logged in as: ', 'wprat' ) . $u->name . '</h3>';
		echo '<br clear="all"><h4><a href="?logout=1">' . __( 'CLICK HERE TO LOG OUT', 'wprat' ) . '</a></h4>';

	}
	//echo '<hr>';
	//echo 'uncomment code below to try some tests.';
	// TESTS /////////////////////////////////////////////

	//echo $oauth_config['uri_user'];

	// GET CURRENT USER DATA
	// Docs : http://wp-api.org/#users_retrieve-current-user

	//echo '<h3>TEST: GET : ' . $oauth_config['uri_user'] . '  to verify current user</h3>';
	//$current_user_object = json_decode( $auth->oauthRequest( $oauth_config['uri_user'], 'GET', $_COOKIE['access_token'], $_COOKIE['access_token_secret'] ) );
	//echo '<h4>RESPONSE:</h4>';
	//echo '<pre>';
	//print_r( $current_user_object );
	//echo '</pre>';
	// exit;

	// TEST : CREATE A NEW POST
	// Docs : http://wp-api.org/#posts_create-a-post
	if ( 'POST' === $method ) {
		$add_title = isset( $_GET['title'] ) ? $_GET['title'] : '';
		$add_body  = isset( $_GET['body'] ) ? $_GET['body'] : '';

		echo '<hr><h3>' . __( 'test : Creating new post' ) . '</h3>';
		echo '<a href="' . admin_url() . 'admin.php?page=wprat_add">' . __( 'Go back', 'wprat' ) . '</a>';
		$post_data = array(
			'status'  => 'draft',
			'title'   => $add_title,
			'content' => $add_body,
		);

		$post_object = $auth->oauthRequest( $oauth_config['wp_api_domain'] . $oauth_config['wp_api_path'] . '/posts', 'POST', $_COOKIE['access_token'], $_COOKIE['access_token_secret'], $post_data );
		echo '<h4>' . __( 'RESPONSE:', 'wprat' ) . '</h4>';
		echo '<pre>';
		print_r( $post_object );
		echo '</pre>';
	}

	// Uncomment the sections below to test some other things..

	/*
	// CREATE NEW ATTACHMENT (Media upload)
	// Docs : http://wp-api.org/#media_create-an-attachment
	echo '<hr><h3>Test : Create New Attachment (Media upload)</h3>';
	$file_data = array(
		'file' => '@./some_photo.jpg;type=image/jpeg'
		//'file' => '@./some_video.m4v;type=video/m4v'
		//'file' => '@./sometune.mp3;type=audio/mpeg'
	);
	$file_object = json_decode($auth->oauthRequest($oauth_config['wp_api_domain'].$oauth_config['wp_api_path'].'/media',
			'POST',
			$_COOKIE['access_token'],
			$_COOKIE['access_token_secret'],
			$file_data
		)
	);
	echo '<h4>UPLOAD RESPONSE:</h4>';
	echo '<pre>';
	print_r($file_object);
	echo '</pre>';
	// Naturally, you'll need to do a file upload first, if you want the attachment to be 'attached' or embedded in a post.
	/**/
	/*
		date
		date_gmt
		password
		slug
		status - One of: publish, future, draft, pending, private
		title
		content
		author
		excerpt
		featured_media
		comment_status -  One of: open, closed
		ping_status - One of: open, closed
		format - One of: standard, aside, chat, gallery, link, image, quote, status, video, audio
		sticky
		categories
		tags
	*/

	// UPDATE A (previous) POST
	if ( 'PUT' === $method ) {
		$up_title = isset( $_GET['title'] ) ? sanitize_text_field( $_GET['title'] ) : '';
		//$up_body    = isset( $oauth_update_options['post_body'] ) ? $oauth_update_options['post_body'] : '';
		$up_id = isset( $_GET['id'] ) ? sanitize_text_field( $_GET['id'] ) : '';

		echo '<hr><h3>' . __( 'Test : Updating Post', 'wprat' ) . '</h3>';
		echo '<a href="' . admin_url() . 'admin.php?page=wprat_update">' . __( 'Go back', 'wprat' ) . '</a>';
		//$post_id = $post_object->id;
		$post_id       = $up_id;
		$update_data   = array(
			//'title' => 'The title was edited immediately after creating it at '.time(),
			'title'   => $up_title,
			'excerpt' => __( 'And this excerpt was added after.', 'wprat' ),
			//'categories' => [ 1, 2, 6 ]
		);
		$update_object = json_decode( $auth->oauthRequest( $oauth_config['wp_api_domain'] . $oauth_config['wp_api_path'] . '/posts/' . $post_id,
			'POST',
			$_COOKIE['access_token'],
			$_COOKIE['access_token_secret'],
			$update_data,
			TRUE  // post as JSON
		)
		);
		echo '<h4>' . __( 'UPDATE POST RESPONSE:', 'wprat' ) . '</h4>';
		echo '<pre>';
		print_r( $update_object );
		echo '</pre>';
		/**/
		// that's it..
	}

	// Delete section.
	if ( 'DELETE' === $method ) {
		$delete_id = isset( $_GET['id'] ) ? sanitize_text_field( $_GET['id'] ) : '';

		echo '<hr><h3>' . __( 'Test : Delete Post', 'wprat'  ) . '</h3>';
		echo '<a href="' . admin_url() . 'admin.php?page=wprat_delete">' . __( 'Go back', 'wprat'  ) . '</a>';
		//$post_id = $post_object->id;
		$post_id = $delete_id;
		/*		$delete_data   = array(
					'id' => $delete_id,
				);*/
		$delete_object = json_decode( $auth->oauthRequest( $oauth_config['wp_api_domain'] . $oauth_config['wp_api_path'] . '/posts/' . $post_id,
			$method,
			$_COOKIE['access_token'],
			$_COOKIE['access_token_secret'],
			'',
			TRUE  // post as JSON
		)
		);
		echo '<pre>';
		print_r( $delete_object );
		echo '</pre>';

	}


} else {
	// Not logged in.
	$request_token_string = $auth->oauthRequest( $oauth_config['uri_request'], 'POST', NULL, NULL );
	parse_str( $request_token_string, $request_parts );
	// temporarily store the oauth_token_secret for the next step after the callback.
	setcookie( "oauth_token_secret", $request_parts['oauth_token_secret'], time() + 60, "/" );
	echo '<h4>' . __( 'request_token_string :', 'wprat'  ) . $request_token_string . '</h4>';
	$insert_url = $oauth_config['uri_authorize'] . '?' . $request_token_string . '&oauth_callback=' . urlencode( $oauth_config['oauth_callback'] );
	$insert_text = __( 'LOGIN USING YOUR ', 'wprat' ) . $oauth_config['wp_api_domain'] . __( ' WORDPRESS ACCOUNT', 'wprat' );
	// Start OAuth authorisation by obtaining a request token and generating a link to the OAuth server, with a callback here ...
	echo '<h3><a href="' . esc_url( $insert_url ) . '">' . esc_html( $insert_text ) . '</a></h3>';
	_e( 'Uses WP-API and OAuth 1.0a Server for WordPress', 'wprat' );
	//var_dump( $auth->uri_authorize );
}


//////////////////////
/*	$token = wp_remote_request( $wp_request_url, array( 'method' => 'GET' ) );

	if ( ! empty( $val_id ) ) {
		$wp_request_url .= $val_id;
	}

	$request_args = array(
		'method'  => $meth,
		'headers' => $wp_request_headers,
	);

	$content_sanitize = array_diff( $content, array( '', NULL, FALSE ) );

	if ( NULL !== $content && is_array( $content ) ) {
		// If delete.
		if ( $content['post_id'] !== '' && count( $content_sanitize ) === 1 ) {
			$request_args['body'] = array(
				'id' => intval( esc_html( $content['post_id'] ) ),
			);
		} // If update.
		elseif ( $content['post_id'] !== '' && count( $content_sanitize ) !== 1 ) {
			$request_args['body'] = array(
				'id'      => intval( esc_html( $content['post_id'] ) ),
				'title'   => isset( $content['post_title'] ) ? esc_html( $content['post_title'] ) : NULL,
				'content' => isset( $content['post_body'] ) ? esc_html( $content['post_body'] ) : NULL,
			);
		} // If add.
		else {
			$request_args['body'] = array(
				'title'   => isset( $content['post_title'] ) ? esc_html( $content['post_title'] ) : NULL,
				'content' => isset( $content['post_body'] ) ? esc_html( $content['post_body'] ) : NULL,
			);
		}
	}
	$wp_delete_post_response = wp_remote_request( $wp_request_url, $request_args );*/

// That's it!
// #########################################################################
// OAuth 1.0a library
// #########################################################################
// NB: All the echo and print_r must be commented out for setcookie to work when running logging in tests.

function logIt( $text ) {

	return;// comment to activate log
	$debugLog = '/path/to/debug/log.txt';
	$t        = print_r( $text, TRUE );
	$text     = $t;
	$handle   = fopen( $debugLog, "a+" );
	if ( fwrite( $handle, $text . "\n" ) === FALSE ) {
	}
	fclose( $handle );
}
