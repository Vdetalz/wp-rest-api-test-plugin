<?php

/**
 * Prepare data for the basic rest request.
 */
function send_rest( array $options, $method = '' ) {
	$args           = wprat_create_arguments( $options );
	$args['method'] = $method;

	$request_options = get_wprat_options( $args );
	$method          = isset( $request_options['method'] ) ? sanitize_text_field( $request_options['method'] ) : NULL;
	$id              = isset( $args['content']['post_id'] ) ? sanitize_text_field( $args['content']['post_id'] ) : NULL;
	$content         = isset( $args['content'] ) ? $args['content'] : NULL;

	if ( ! $request_options['method'] ) {
		return;
	}

	if ( 'basic' === $request_options['type_auth'] ) {
		send_rest_basic( $method, $id, $content );
	}

}


/**
 * Send request with Basic Authenification.
 */
function send_rest_basic( $method = 'POST', $id = '', array $content = NULL ) {

	$val_op  = get_option( 'wprat_options_name' );
	$address = isset( $val_op ) ? sanitize_text_field( $val_op['remote_address'] ) : NULL;
	$name    = isset( $val_op ) ? sanitize_text_field( $val_op['remote_name'] ) : NULL;
	$pass    = isset( $val_op ) ? sanitize_text_field( $val_op['remote_pass'] ) : NULL;
	$meth    = isset( $method ) ? sanitize_text_field( $method ) : NULL;

	$wp_request_headers = array(
		'Authorization' => 'Basic ' . base64_encode( $name . ':' . $pass ),
	);

	$val_id = trim( $id );

	$wp_request_url = 'http://' . $address . '/wp-json/wp/v2/posts/';

	if ( ! empty( $val_id ) ) {
		$wp_request_url .= $val_id;
	}

	$request_args = array(
		'method'  => $meth,
		'headers' => $wp_request_headers,
	);

	$content_sanitize = array_diff( $content, array( '', NULL, FALSE ) );

	if ( NULL !== $content && is_array( $content ) ) {
		// If delete.
		if ( '' !== $content['post_id'] && 1 === count( $content_sanitize ) ) {
			$request_args['body'] = array(
				'id' => intval( sanitize_text_field( $content['post_id'] ) ),
			);
		} // If update.
		elseif ( '' !== $content['post_id'] && 1 !== count( $content_sanitize ) ) {
			$request_args['body'] = array(
				'id'      => intval( sanitize_text_field( $content['post_id'] ) ),
				'title'   => isset( $content['post_title'] ) ? sanitize_text_field( $content['post_title'] ) : NULL,
				'content' => isset( $content['post_body'] ) ? sanitize_text_field( $content['post_body'] ) : NULL,
			);
		} // If add.
		else {
			$request_args['body'] = array(
				'title'   => isset( $content['post_title'] ) ? sanitize_text_field( $content['post_title'] ) : NULL,
				'content' => isset( $content['post_body'] ) ? sanitize_text_field( $content['post_body'] ) : NULL,
			);
		}
	}
	$wp_delete_post_response = wp_remote_request( $wp_request_url, $request_args );
}
