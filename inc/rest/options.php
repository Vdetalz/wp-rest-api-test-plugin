<?php

/**
 * @return bool
 */
function check_basic_auth() {
	$type = get_option( 'wprat_options_name' ) ? get_option( 'wprat_options_name' ) : FALSE;
	if ( isset( $type['type_auth'] ) && 'basic' === $type['type_auth'] ) {
		return TRUE;
	}
	return FALSE;
}

/**
 * Create arguments for callback on settings.
 *
 * @param array $options
 *
 * @return array
 */
function wprat_create_arguments( array $options ) {
	// There are data for remote manipulation.
	$names = get_remote_data();

	$content = [ ];

	foreach ( $names as $name => $val ) {
		if ( array_key_exists( $name, $options ) ) {
			$content[ $name ] = sanitize_text_field( $options[ $name ] );
		} else {
			$content[ $name ] = '';
		}
	}

	$args = array(
		'content' => $content,
	);

	return $args;
}

/**
 * Get plugin options, check possible values.
 */
function get_wprat_options( $arg_options ) {
	$val_op   = get_option( 'wprat_options_name' );
	$requests = get_methods();

	$wprat_options = array(
		'address'     => isset( $val_op ) ? $val_op['remote_address'] : NULL,
		'name'        => isset( $val_op ) ? $val_op['remote_name'] : NULL,
		'pass'        => isset( $val_op ) ? $val_op['remote_pass'] : NULL,
		'method'      => in_array( $arg_options['method'], $requests, TRUE ) ? $arg_options['method'] : NULL,
		'type_auth'   => isset( $val_op ) ? $val_op['type_auth'] : NULL,
		'key'         => isset( $val_op ) ? $val_op['key'] : NULL,
		'secret'      => isset( $val_op ) ? $val_op['secret'] : NULL,
		'uri_request' => isset( $val_op ) ? $val_op['uri_request'] : NULL,
		'wp_api_path' => isset( $val_op ) ? $val_op['wp_api_path'] : NULL,
	);

	return $wprat_options;
}

/**
 * There are customize data for remote manipulation.
 *
 * @return array
 */
function get_remote_data() {
	return array(
		'post_body'  => '',
		'post_title' => '',
		'post_id'    => '',
	);
}

/**
 * There are customize request methods.
 *
 * @return array
 */
function get_methods() {
	return array(
		'GET',
		'POST',
		'PUT',
		'HEAD',
		'DELETE',
	);
}
