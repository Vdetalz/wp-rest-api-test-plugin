<?php
/**
 * Plugin Name: WP REST API TEST
 * Description: TESTING REST API.
 * Version: 0.1
 */

include_once('inc/rest/rest.php');
include_once('inc/rest/options.php');

/**
 * Create plugin page settings.
 */
function add_wprat_page() {
	add_menu_page( __( 'Options WPRAT', 'wprat' ), __( 'Options WPRAT', 'wprat' ), 'manage_options', 'wprat_options' );
	add_submenu_page( 'wprat_options', __( 'Main Options', 'wprat' ), __( 'Main Options', 'wprat' ), 'manage_options', 'wprat_options', 'wprat_options_page_output' );
	add_submenu_page( 'wprat_options', __( 'Delete content', 'wprat' ), __( 'Delete Content', 'wprat' ), 'manage_options', 'wprat_delete', 'wprat_delete_page_output' );
	add_submenu_page( 'wprat_options', __( 'Update content', 'wprat' ), __( 'Update Content', 'wprat' ), 'manage_options', 'wprat_update', 'wprat_update_page_output' );
	add_submenu_page( 'wprat_options', __( 'Add content', 'wprat' ), __( 'Add Content', 'wprat' ), 'manage_options', 'wprat_add', 'wprat_add_page_output' );
}

add_action( 'admin_menu', 'add_wprat_page' );

/**
 * Output content.
 */
function wprat_options_page_output() {
	?>
	<div class="wrap">
		<form action="options.php" method="POST">
			<?php
			settings_fields( 'wprat_options_group' );
			do_settings_sections( 'wprat_options' );
			submit_button();
			?>
		</form>
	</div>
	<?php
}

function wprat_delete_page_output() {
	?>
	<div class="wrap">
		<form action="options.php" method="POST">
			<?php
			settings_fields( 'wprat_delete_group' );
			do_settings_sections( 'wprat_delete' );
			submit_button();
			?>
		</form>
	</div>
	<?php
}

function wprat_update_page_output() {
	?>
	<div class="wrap">
		<form action="options.php" method="POST">
			<?php
			settings_fields( 'wprat_update_group' );
			do_settings_sections( 'wprat_update' );
			submit_button();
			?>
		</form>
	</div>
	<?php
}

function wprat_add_page_output() {
	?>
	<div class="wrap">
		<form action="options.php" method="POST">
			<?php
			settings_fields( 'wprat_add_group' );
			do_settings_sections( 'wprat_add' );
			submit_button();
			?>
		</form>
	</div>
	<?php
}

/**
 * Registering settings.
 */
function wprat_settings() {
	register_setting( 'wprat_options_group', 'wprat_options_name', array(
		'sanitize_callback' => 'options_callback',
	) );
	register_setting( 'wprat_delete_group', 'wprat_post_delete_name', array(
		'sanitize_callback' => 'wprat_delete_group',
	) );
	register_setting( 'wprat_update_group', 'wprat_post_update_name', array(
		'sanitize_callback' => 'wprat_update_group',
	) );
	register_setting( 'wprat_add_group', 'wprat_post_add_name', array(
		'sanitize_callback' => 'wprat_add_group',
	) );

	add_settings_section( 'section_options', __( 'Main options WP Rest API Test Plugin', 'wprat' ), '', 'wprat_options' );
	add_settings_field( 'wprat_remote_address', __( 'Remote site settings', 'wprat' ), 'fill_remote_address', 'wprat_options', 'section_options' );

	add_settings_section( 'section_delete', __( 'Delete Request Options', 'wprat' ), '', 'wprat_delete' );
	add_settings_field( 'wprat_delete_post', __( 'Delete data post', 'wprat' ), 'fill_post_delete', 'wprat_delete', 'section_delete' );

	add_settings_section( 'section_update', __( 'Update Request Options', 'wprat' ), '', 'wprat_update' );
	add_settings_field( 'wprat_update_post', __( 'Update data post', 'wprat' ), 'fill_post_update', 'wprat_update', 'section_update' );

	add_settings_section( 'section_add', __( 'Add Request Options', 'wprat' ), '', 'wprat_add' );
	add_settings_field( 'wprat_add_post', __( 'Add data post', 'wprat' ), 'fill_post_add', 'wprat_add', 'section_add' );

}

add_action( 'admin_init', 'wprat_settings' );

/**
 * Output data Main Option.
 */
function fill_remote_address() {
	$val               = get_option( 'wprat_options_name' );
	$val_a             = isset( $val ) ? $val['remote_address'] : NULL;
	$val_n             = isset( $val ) ? $val['remote_name'] : NULL;
	$val_p             = isset( $val ) ? $val['remote_pass'] : NULL;
	$val_key           = isset( $val ) ? $val['key'] : NULL;
	$val_secret        = isset( $val ) ? $val['secret'] : NULL;
	$val_wp_api_domain = isset( $val ) ? $val['uri_request'] : NULL;
	$val_type_auth     = isset( $val ) ? $val['type_auth'] : NULL;
	?>
	<table>
		<tr>
			<td colspan="2" align="center">
				<strong><?php esc_html_e( 'Active Type of Authenification', 'wprat' ); ?></strong>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Basic: ', 'wprat' ); ?>
			</th>
			<td><input type="radio"
			           name="wprat_options_name[type_auth]" <?php if ( 'basic' === $val_type_auth ) {
					echo 'checked';
				}
				?>
			           value="basic"/></td>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'OAuth: ', 'wprat' ); ?>
			</th>
			<td><input type="radio"
			           name="wprat_options_name[type_auth]" <?php if ( 'oauth' === $val_type_auth ) {
					echo 'checked';
				} ?>
			           value="oauth"/></td>
		</tr>

		<tr>
			<td colspan="2" align="center">
				<strong><?php esc_html_e( 'Basic Authenification', 'wprat' ); ?></strong>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Uri Request: ', 'wprat' ); ?>
			</th>
			<td><input type="text" placeholder="example.com" name="wprat_options_name[remote_address]"
			           value="<?php echo esc_attr( $val_a ); ?>"/>
				<p class="description"><?php esc_html_e( 'Your site domain name' ) ?></p></td>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Login: ', 'wprat' ); ?></th>
			<td><input type="text" name="wprat_options_name[remote_name]"
			           value="<?php echo esc_attr( $val_n ); ?>"/></td>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Password: ', 'wprat' ); ?></th>
			<td><input type="text" name="wprat_options_name[remote_pass]"
			           value="<?php echo esc_attr( $val_p ); ?>"/></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<strong><?php esc_html_e( 'OAuth 1.0 Authenification', 'wprat' ); ?></strong>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Client Key: ', 'wprat' ); ?>
			</th>
			<td><input type="text" name="wprat_options_name[key]"
			           value="<?php echo esc_attr( $val_key ); ?>"/></td>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Client Secret:', 'wprat' ); ?>
			</th>
			<td><input type="text" name="wprat_options_name[secret]"
			           value="<?php echo esc_attr( $val_secret ); ?>"/></td>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Uri Request: ', 'wprat' ); ?></th>
			<td><input type="text" placeholder="example.com" name="wprat_options_name[uri_request]"
			           value="<?php echo esc_attr( $val_wp_api_domain ); ?>"/>
				<p class="description"><?php esc_html_e( 'Your site domain name' ) ?></p></td>
		</tr>
	</table>
	<?php
}

/**
 * Output data Delete Option.
 */
function fill_post_delete() {
	$val    = get_option( 'wprat_post_delete_name' );
	$method = 'DELETE';
	$val_id = isset( $val ) ? $val['post_id'] : NULL;
	?>
	<table>
		<tr>
			<th scope="row"><?php esc_html_e( 'Post ID: ', 'wprat' ); ?></th>
			<td><input type="text" name="wprat_post_delete_name[post_id]"
			           value="<?php esc_attr_e( trim( $val_id ) ); ?>"/></td>
		</tr>
		<?php
			echo view_oauth_link( $val, $method );
		?>
	</table>
	<?php
}

/**
 * Output data Update Option.
 */
function fill_post_update() {
	$val       = get_option( 'wprat_post_update_name' );
	$method    = 'PUT';
	$val_id    = isset( $val ) ? $val['post_id'] : NULL;
	$val_title = isset( $val ) ? $val['post_title'] : NULL;
	?>
	<table>
		<tr>
			<th scope="row"><?php esc_html_e( 'ID Post: ', 'wprat' ); ?></th>
			<td><input type="text" name="wprat_post_update_name[post_id]"
			           value="<?php esc_attr_e( trim( $val_id ) ); ?>"/></td>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'New Title: ', 'wprat' ); ?></th>
			<td>
				<input type="text" name="wprat_post_update_name[post_title]"
				       value="<?php esc_attr_e( trim( $val_title ) ); ?>"/>
			</td>
		</tr>
		<?php
			echo view_oauth_link( $val, $method );
		?>
	</table>

	<?php
}

/**
 * Output data Add Option.
 */
function fill_post_add() {
	$val       = get_option( 'wprat_post_add_name' );
	$method    = 'POST';
	$val_title = isset( $val['post_title'] ) ? $val['post_title'] : NULL;
	$val_body  = isset( $val['post_body'] ) ? $val['post_body'] : NULL;
	?>
	<table>
		<tr>
			<th scope="row"><?php esc_html_e( 'Post Title', 'wprat' ); ?></th>
			<td><input type="text" name="wprat_post_add_name[post_title]"
			           value="<?php esc_attr_e( trim( $val_title ) ); ?>"/>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php esc_html_e( 'Post Body', 'wprat' ); ?></th>
			<td><textarea
					name="wprat_post_add_name[post_body]"><?php echo sanitize_textarea_field( $val_body ); ?></textarea>
			</td>
		</tr>
		<?php
			echo view_oauth_link( $val, $method );
		?>
	</table>

	<?php
}

/**
 * Sanitize Main option page callback.
 */
function options_callback( $options ) {
	foreach ( $options as $name => &$val ) {
		if ( 'type_auth' === $name ) {
			$val = sanitize_text_field( $val );
		}
		if ( 'remote_address' === $name ) {
			$val = sanitize_text_field( $val );
		}

		if ( 'remote_name' === $name ) {
			$val = sanitize_text_field( $val );
		}

		if ( 'remote_pass' === $name ) {
			$val = sanitize_text_field( $val );
		}

		// OAuth.
		if ( 'key' === $name ) {
			$val = sanitize_text_field( $val );
		}
		if ( 'secret' === $name ) {
			$val = sanitize_text_field( $val );
		}
		if ( 'uri_request' === $name ) {
			$val = sanitize_text_field( $val );
		}
	}

	return $options;
}

/**
 * Sanitize Delete page callback and send REST request with basic authenification.
 */
function wprat_delete_group( $options ) {
	if ( check_basic_auth() ) {
		send_rest( $options, 'DELETE' );
	}

	return $options;
}

/**
 * Sanitize Update page callback and send REST request with basic authenification.
 *
 * @param array $options
 *
 * @return array
 */
function wprat_update_group( $options ) {
	if ( check_basic_auth() ) {
		send_rest( $options, 'PUT' );
	}

	return $options;
}

/**
 * Sanitize Add page callback and send REST request with basic authenification.
 */
function wprat_add_group( $options ) {
	if ( check_basic_auth() ) {
		send_rest( $options, 'POST' );
	}

	return $options;
}

/**
 * View Link to oauth request if it chosen.
 */
function view_oauth_link( $val, $method ) {

	$id    = isset( $val['post_id'] ) ? sanitize_text_field( $val['post_id'] ) : '';
	$title = isset( $val['post_title'] ) ? sanitize_text_field( $val['post_title'] ) : '';
	$body  = isset( $val['post_body'] ) ? sanitize_text_field( $val['post_body'] ) : '';

	$options = get_option( 'wprat_options_name' );
	$auth    = $options['type_auth'];

	if ( isset( $auth ) && 'oauth' === $auth ) {
		$link = '<tr><th scope="row">';
		$link .= '<a href="' . plugins_url( "wp-api-oauth-test-client.php?id=$id&title=$title&body=$body&method=$method", __FILE__ ) . '">' . __( 'Create request', 'wprat' ) . '</a>';
		$link .= '</th><td><p class="description">' . __( 'You will ba able to send request after save changes.' ) . '</p></td></tr>';

		return $link;
	}

	return FALSE;
}
